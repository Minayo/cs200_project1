#include <iostream>
#include <string>
using namespace std;
//This is for TLLab 2. I uplpoaded Project 1 file
int main()
{
	int hunger = 0;
	int health = 100;
	int happiness = 100;
	string petName;
	
	cout << "Please enter yout pet's name: ";
	cin >> petName;
	bool isDone = false;
	int menuChoice;
	while (!isDone)
	{
		cout << "\n---------------------------------------------------------" << endl;
		cout << petName << endl;
		cout << "         Hunger: " << hunger << "%" << "     Health: " << health << "%" << "     Happiness: " << happiness << "%" << endl;
		cout << "---------------------------------------------------------" << endl;
		
		cout << "\nOPTIONS: 1. Feed  2. Play  3. Vet  4. Quit" << endl;
		cout << ">";
		cin >> menuChoice;

		hunger += 5;
		if (hunger >= 50)
		{
			happiness = happiness - 10;
			health = health - 10;
		} else 
		{
			happiness = happiness - 5;
		}

		switch (menuChoice)
		{
		case 3:
			cout << "\nThe vet says:" << endl;
			if (happiness <= 50)
			{
				cout << "-Make sure to play with " << petName << " more." << endl;
			}
			if (hunger >= 50)
			{
				cout << "-Make sure to feed " << petName << endl;
			}
			if (health <= 50)
			{
				cout << "-" << petName << "isn't looking healthy. Take better care of it!" << endl;
			}
			if (happiness > 50 && hunger < 50 && health >50)
			{
				cout << "-" << petName << " is looking OK!" << endl;
			}
			break;
		case 4:
			cout << "\nAre you sure you want to quit?" << endl;
			cout << "1. QUIT        2. Don't quit" << endl;
			cout << ">";
			cin >> menuChoice;
			if (menuChoice == 1)
			{
				isDone = true;
			}
			break;
		case 1:
			cout << "\nFOODS: 1. Pizza  2. Broccoli  3. Tuna" << endl;
			cout << ">";
			cin >> menuChoice;
			if (menuChoice == 1)
			{
				health = health - 1;
				hunger = hunger - 15;
				cout << "\nYou feed pizza to " << petName << endl;
			}
			else if (menuChoice == 2)
			{
				health = health + 1;
				hunger = hunger - 10;
				cout << "\nYou feed broccoli to " << petName << endl;
			}
			else if (menuChoice == 3)
			{
				health = health + 2;
				hunger = hunger - 12;
				cout << "\nYou feed Tuna to " << petName << endl;
			}
			break;
		case 2:
			cout << "\nGAMES: 1. Fetch  2. Tug-of-war  3. Videogame" << endl;
			cout << ">";
			cin >> menuChoice;
			if (menuChoice == 1)
			{
				health = health + 2;
				happiness = happiness + 8;
				cout << "\nYou play fetch with " << petName << endl;
			}
			else if (menuChoice == 2)
			{
				happiness = happiness + 9;
				cout << "\nYou play tug-of-war with " << petName << endl;
			}
			else if (menuChoice == 3)
			{
				health = health - 1;
				happiness = happiness + 10;
				cout << "\nYou play Videogame with " << petName << endl;
			}
		}

		if (happiness <= 0) { happiness = 0; }
		else if (happiness >= 100) { happiness = 100; }

		if (hunger <= 0) { hunger = 0; }
		else if (hunger >= 100) { hunger = 100; }

		if (health <= 0) 
		{ 
			health = 0; 
			isDone = true;
			cout << "\nYou haven't taken care of " << petName << "!" << endl;
			cout << petName << " has been removed from your care." << endl;
		}
		else if (health >= 100) { health = 100; }

	

	}
	return 0;
}
